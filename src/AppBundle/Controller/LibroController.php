<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Libro;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Libro controller.
 *
 * @Route("libros")
 */
class LibroController extends Controller
{
    /**
     * Lists all libro entities.
     *
     * @Route("/", name="libros_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $libros = $em->getRepository('AppBundle:Libro')->findAll();

        return $this->render('libro/index.html.twig', array(
            'libros' => $libros,
        ));
    }

    /**
     * Creates a new libro entity.
     *
     * @Route("/new", name="libros_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $libro = new Libro();
        $form = $this->createForm('AppBundle\Form\LibroType', $libro);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($libro);
            $em->flush();

            return $this->redirectToRoute('libros_show', array('id' => $libro->getId()));
        }

        return $this->render('libro/new.html.twig', array(
            'libro' => $libro,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a libro entity.
     *
     * @Route("/{id}", name="libros_show")
     * @Method("GET")
     */
    public function showAction(Libro $libro)
    {
        $deleteForm = $this->createDeleteForm($libro);

        return $this->render('libro/show.html.twig', array(
            'libro' => $libro,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing libro entity.
     *
     * @Route("/{id}/edit", name="libros_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Libro $libro)
    {
        $deleteForm = $this->createDeleteForm($libro);
        $editForm = $this->createForm('AppBundle\Form\LibroType', $libro);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('libros_edit', array('id' => $libro->getId()));
        }

        return $this->render('libro/edit.html.twig', array(
            'libro' => $libro,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a libro entity.
     *
     * @Route("/{id}", name="libros_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Libro $libro)
    {
        $form = $this->createDeleteForm($libro);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($libro);
            $em->flush();
        }

        return $this->redirectToRoute('libros_index');
    }

    /**
     * Creates a form to delete a libro entity.
     *
     * @param Libro $libro The libro entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Libro $libro)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('libros_delete', array('id' => $libro->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
